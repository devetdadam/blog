Title: Bouger un objet de type TextScript
Date: 2018-03-15
Tags: lilypond, définitions, TextScript
Summary: Fonction qui permet de positionner un opbjet TextScript

Deux possibilités : 

* uniquement dans l'axe Y
* horizontalement et verticalement

``` 
\version "2.19.58"
	
%%% Fonction
moveTextScript = #(define-music-function
        	(parser location a-b)
        	(pair?)
        	#{
            	\once \override TextScript.extra-offset = $a-b
        	#}
        )
        
	
    %%% Usage
    \relative c''{
        c2 d e1^"texte" \break 
        c2 d \moveTextScript  #'(2 . -4.5) g1^"texte"
        \bar "|."
        }
```

![Fonction qui permet de positionner un opbjet TextScript]({filename}/images/moveText.png)

*NB : Cette syntaxe est aussi valable avec les objets de type Script ou tout autre opérant la méthode extra-offset*