#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Jean-Marc Legrand'
SITENAME = 'Pour mémoire...'
SITEURL = ''

PATH = 'content'
STATIC_PATHS = ['images', 'extra/faicon.ico']
EXTRA_PATH_METADATA = {
 'extra/favicon.ico': { 'path': 'favicon.ico' },
}

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'
USE_FOLDER_AS_CATEGORY = True

THEME = 'notmyidea'
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Only one author
AUTHOR_URL = False
AUTHOR_SAVE_AS = False
AUTHORS_SAVE_AS = False

# Tag cloud
TAG_CLOUD_STEPS = 4
TAG_CLOUD_MAX_ITEMS = 30

# Blogroll
LINKS = (('Éditions IN NOMINE', 'http://www.editionsinnomine.com/'),
         ('DEV & d\'Adam', 'http://www.devetdadam.com/'),
         )

# Social widget
SOCIAL = (('Mon Facebook', 'https://www.facebook.com/jeanmarc.legrand.50'),
          ('Page In NOMINE', 'https://www.facebook.com/editionsinnomine/'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Coloration syntaxique
MD_EXTENSIONS = [ 'codehilite(css_class=highlight,linenums=False)' ]
